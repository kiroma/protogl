#include <iostream>
#include "gl3w/gl3w.h"
#include <SFML/Graphics/Image.hpp>
#include <SFML/Window.hpp>
#include "pgl/pgl.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <chrono>
#include <vector>

template<typename T>
std::vector<T> readVertices(const std::string &file)
{
	std::ifstream ifs(file);
	std::vector<T> result;
	T input;
	while(ifs >> input)
	{
		result.push_back(input);
	}
	return result;
}

class Clock
{
	std::chrono::steady_clock::time_point tp;
public:
	Clock() : tp(std::chrono::steady_clock::now()) {}
	decltype(tp - std::chrono::steady_clock::now()) elapsed()
	{
		return std::chrono::steady_clock::now() - tp;
	}
	decltype(tp - std::chrono::steady_clock::now()) reset()
	{
		auto cur = std::chrono::steady_clock::now();
		auto result = cur - tp;
		tp = cur;
		return result;
	}
};

class posBuffer
{
	size_t bufsize = 0;
	pgl::ssbo buffer;
public:
	posBuffer(const GLuint index)
	{
		buffer.bindBufferBase(index);
	}
	template<typename T>
	void update(const std::vector<T> &positions)
	{
		buffer.bind();
		if(positions.capacity() * sizeof(T) > bufsize)
		{
			buffer.reserve<T>(positions.capacity(), GL_DYNAMIC_DRAW);
			buffer.bufferSubData<T>(positions, 0);
			bufsize = positions.capacity() * sizeof(T);
		}
		else
		{
			buffer.bufferSubData<T>(positions, 0);
		}
	}
};

int main()
{
	std::cout.sync_with_stdio(false);
	sf::ContextSettings settings(24, 0, 16, 4, 6, sf::ContextSettings::Attribute::Core);
#ifndef NDEBUG
	settings.attributeFlags |= sf::ContextSettings::Attribute::Debug;
#endif //NDEBUG
	sf::Window window(sf::VideoMode::getDesktopMode(), "ProtoGL", sf::Style::Fullscreen, settings);
	unsigned int FPSlimit = 0;
	window.setFramerateLimit(FPSlimit);
	if (gl3wInit()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}
	if(!gl3wIsSupported(4, 6))
	{
		std::cerr << "OpenGL 4.6 is not supported!" << std::endl;
		return -1;
	}
	window.setMouseCursorGrabbed(true);
	window.setMouseCursorVisible(false);
#ifndef NDEBUG
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#else
	glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif //NDEBUG
	glDebugMessageCallback(pgl::MessageCallback, NULL);
	pgl::vao vao;
	vao.bind();
	pgl::vbo vbo;

	std::vector<GLfloat> vertices {
		-0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
		-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f
	};
	vbo.bufferData<GLfloat>(vertices, GL_STATIC_DRAW);

	std::vector<GLuint> elements {
		0, 1, 2,
		2, 1, 3,
		5, 4, 6,
		5, 6, 7,
		1, 0, 4,
		4, 5, 1,
		7, 6, 2,
		2, 3, 7,
		3, 5, 7,
		1, 5, 3,
		4, 2, 6,
		0, 2, 4
	};
	pgl::ebo ebo;
	ebo.bufferData<GLuint>(elements, GL_STATIC_DRAW);

	pgl::Shader vertexShader(GL_VERTEX_SHADER);
	pgl::Shader fragmentShader(GL_FRAGMENT_SHADER);
#ifdef USE_SPIRV
	vertexShader.loadBinaryFromFile("res/shader.spv");
	fragmentShader.loadBinaryFromFile("res/shader.spv");
#else
	vertexShader.loadFromFile("res/shader.vert");
	fragmentShader.loadFromFile("res/shader.frag");
#endif

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	GLuint tex;
	glGenTextures(1, &tex);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);
	{
		sf::Image texture_data;
		texture_data.loadFromFile("res/brick.jpg");
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture_data.getSize().x, texture_data.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.getPixelsPtr());
	}
	glGenerateMipmap(GL_TEXTURE_2D);

	pgl::Program program = pgl::ProgramBuilder()
		.attach(vertexShader)
		.attach(fragmentShader)
		.link();
	program.use();
	program.setAttrib<GLfloat>("vPos", 3, GL_FLOAT, GL_FALSE, 8, 0);
	program.setAttrib<GLfloat>("vCol", 3, GL_FLOAT, GL_FALSE, 8, 3);
	program.setAttrib<GLfloat>("texPos", 2, GL_FLOAT, GL_FALSE, 8, 6);
	GLint projection_loc = program.getUniformLocation("Projection");
	GLint view_loc = program.getUniformLocation("View");
	GLint model_loc = program.getUniformLocation("Model");
	GLint max_attribs;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &max_attribs);
	std::cout << "Max attributes: " << max_attribs << std::endl;
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);

	std::vector<glm::vec4> cubePoses{};
	glm::mat4 Projection = glm::perspective(45.0f, static_cast<float>(window.getSize().x) / window.getSize().y, 0.1f, 1000.0f);
	glUniformMatrix4fv(projection_loc, 1, GL_FALSE, glm::value_ptr(Projection));
	glm::vec3 camPos(0.0f, 0.0f, 20.0f);
	glm::vec3 camTarget(13.0f, 13.0f, 0.0f);
	glm::vec3 camDirection = glm::normalize(camPos - camTarget);
	glm::vec3 camFront = glm::vec3(0.0f, 0.0f, -1.0f);
	const glm::vec3 globalUp = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 camRight = glm::normalize(glm::cross(globalUp, camDirection));
	glm::mat4 View = glm::lookAt(camPos, camPos + camFront, globalUp );
	glm::mat4 Model = glm::mat4(1.0);
	Model = glm::scale(glm::mat4(1.0f), glm::vec3(1.5f));
	glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(Model));
	Clock clock;
	Clock windowClock;
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	posBuffer positions(0);
	float yaw = -90.0f;
	float pitch = 0.0f;
	bool cubesUpdated = false;
	while(window.isOpen())
	{
		float timeDelta = std::chrono::duration_cast<std::chrono::duration<float>>(clock.reset()).count();
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					switch(event.key.code)
					{
						case sf::Keyboard::Escape:
							window.close();
							break;
						case sf::Keyboard::R:
							cubePoses.clear();
						default:
							break;
					}
					break;
				case sf::Event::Resized:
					glViewport(0, 0, window.getSize().x, window.getSize().y);
					Projection = glm::perspective(45.0f, static_cast<float>(window.getSize().x) / window.getSize().y, 0.1f, 100.0f);
					glUniformMatrix4fv(projection_loc, 1, GL_FALSE, glm::value_ptr(Projection));
					break;
				default:
					break;
			}
		}
		if(window.hasFocus()) // Mouse input
		{
			const float sensitivity = 0.04f;
			yaw += (sf::Mouse::getPosition(window).x - static_cast<int>(window.getSize().x/2)) * sensitivity;
			pitch += (static_cast<int>(window.getSize().y/2) - sf::Mouse::getPosition(window).y) * sensitivity;
			sf::Mouse::setPosition(sf::Vector2i(window.getSize().x/2, window.getSize().y/2), window);
			if(pitch > 89.0f)
			{
				pitch = 89.0f;
			}
			if(pitch < -89.0f)
			{
				pitch = -89.0f;
			}
			camFront.x = std::cos(glm::radians(yaw)) * std::cos(glm::radians(pitch));
			camFront.y = std::sin(glm::radians(pitch));
			camFront.z = std::sin(glm::radians(yaw)) * std::cos(glm::radians(pitch));
			camFront = glm::normalize(camFront);

			float speed = 5.0f; // Keyboard input
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
			{
				speed *= 2;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				camPos += camFront * speed * timeDelta;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				camPos -= camFront * speed * timeDelta;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				camPos -= glm::normalize(glm::cross(camFront, globalUp)) * speed * timeDelta;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				camPos += glm::normalize(glm::cross(camFront, globalUp)) * speed * timeDelta;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				camPos += globalUp * speed * timeDelta;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
			{
				camPos -= globalUp * speed * timeDelta;
			}
		}

		View = glm::lookAt(camPos, camPos + camFront, globalUp);
		glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(View));

		float secs = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::steady_clock::now().time_since_epoch()).count();
		Model = glm::rotate(Model, glm::radians(90.0f) * timeDelta, glm::vec3(std::sin(secs), std::cos(secs), 0.0f));
		glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(Model));
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			cubesUpdated = true;
			cubePoses.emplace_back(camPos + (camFront * 2.0f), 1.0f);
		}
		if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			cubesUpdated = true;
			cubePoses.emplace_back(camPos - (camFront * 2.0f), 1.0f);
		}
		if(cubesUpdated)
		{
			positions.update(cubePoses);
			cubesUpdated = false;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawElementsInstanced(GL_TRIANGLES, elements.size(), GL_UNSIGNED_INT, 0, cubePoses.size());
		glFlush();
		window.display();
		glFinish();
	}
	std::cout << "Cube count: " << cubePoses.size() << '\n';
	return 0;
}

#include "../gl3w/gl3w.h"
#include <string_view>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#pragma once

namespace pgl
{
	void MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void*);

	class vao
	{
	public:
		vao()
		{
			glGenVertexArrays(1, &name);
		}
		~vao()
		{
			glDeleteVertexArrays(1, &name);
		}
		void bind()
		{
			glBindVertexArray(name);
		}
	private:
		GLuint name;
	};

	namespace priv
	{
		class BufferBase
		{
		public:
			BufferBase(GLenum target) : target(target)
			{
				glGenBuffers(1, &buffer);
			}
			BufferBase(BufferBase&) = delete;
			BufferBase(BufferBase&&) = default;
			~BufferBase()
			{
				glDeleteBuffers(1, &buffer);
			}
			void bind()
			{
				glBindBuffer(target, buffer);
			}
			template<typename T>
			void bufferData(const std::vector<T> &data, const GLenum usage)
			{
				bind();
				glBufferData(target, data.size()*sizeof(T), data.data(), usage);
			}
			template<typename T>
			void reserve(const size_t new_size, const GLenum usage)
			{
				bind();
				glBufferData(target, new_size*sizeof(T), NULL, usage);
			}
			template<typename T>
			void bufferSubData(const std::vector<T> &data, const GLintptr offset)
			{
				bind();
				glBufferSubData(target, offset, data.size() * sizeof(T), data.data());
			}
		protected:
			const GLenum target;
			GLuint buffer;
		};
	}

	class vbo : public priv::BufferBase
	{
	public:
		vbo() : BufferBase(GL_ARRAY_BUFFER) {}
	};

	class ebo : public priv::BufferBase
	{
	public:
		ebo() : BufferBase(GL_ELEMENT_ARRAY_BUFFER) {}
	};
	
	class ssbo : public priv::BufferBase
	{
	public:
		ssbo() : BufferBase(GL_SHADER_STORAGE_BUFFER) {}
		void bindBufferBase(const GLuint index)
		{
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer);
		}
	};
	
	class Shader
	{
		friend class ProgramBuilder;
	public:
		Shader(GLenum shaderType) : shaderType(shaderType)
		{
			shader = glCreateShader(shaderType);
		}
		~Shader()
		{
			glDeleteShader(shader);
		}
		void loadFromFile(const std::string &file)
		{
			std::ifstream ifs(file);
			if(!ifs.is_open())
			{
				throw std::runtime_error(std::string("Could not open ") + file);
			}
			std::stringstream ss;
			ss << ifs.rdbuf();
			std::string source = ss.str();
			const GLchar* sourceptr = source.c_str();
			const GLint sourcelenght = source.length();
			glShaderSource(shader, 1, &sourceptr, &sourcelenght);
			glCompileShader(shader);
			GLint logLength;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
			if(logLength > 0)
			{
				std::string shaderlog(logLength, 0);
				glGetShaderInfoLog(shader, logLength, 0, &shaderlog[0]);
				std::cout << "Shader compilation log: " << shaderlog << std::endl;
			}
		}
		void loadBinaryFromFile(const std::string &file)
		{
			std::ifstream ifs(file);
			if(!ifs.is_open())
			{
				throw std::runtime_error(std::string("Could not open ") + file);
			}
			std::stringstream ss;
			ss << ifs.rdbuf();
			std::string source = ss.str();
			const GLchar* sourceptr = source.c_str();
			const GLint sourcelenght = source.length();
			glShaderBinary(1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V, sourceptr, sourcelenght);
			glSpecializeShader(shader, "main", 0, nullptr, nullptr);
			GLint logLength;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
			if(logLength > 0)
			{
				std::string shaderlog(logLength, 0);
				glGetShaderInfoLog(shader, logLength, 0, &shaderlog[0]);
				std::cout << "Shader compilation log: " << shaderlog << std::endl;
			}
		}
	private:
		GLuint shader;
		const GLenum shaderType;
	};
	
	class ProgramBuilder
	{
		friend class Program;
	public:
		ProgramBuilder() : program(glCreateProgram()) {}
		ProgramBuilder& attach(const Shader &shader)
		{
			glAttachShader(program, shader.shader);
			return *this;
		}
		ProgramBuilder& link()
		{
			glLinkProgram(program);
			return *this;
		}
	private:
		const GLuint program;
	};
	
	class Program
	{
	public:
		Program() = delete;
		Program(ProgramBuilder builder) : program(builder.program) {}
		Program(Program &) = delete;
		Program(Program &&other) : program(other.program)
		{
			other.program = 0;
		}
		~Program()
		{
			glDeleteProgram(program);
		}
		void use()
		{
			glUseProgram(program);
		}
		template<typename T>
		void setAttrib(const std::string &attrib, const GLint size, const GLenum type, const GLboolean normalized, const GLsizei stride, const size_t offset)
		{
			GLint loc = glGetAttribLocation(program, attrib.c_str());
			glEnableVertexAttribArray(loc);
			switch(type)
			{
				case GL_BYTE:
				case GL_UNSIGNED_BYTE:
				case GL_SHORT:
				case GL_UNSIGNED_SHORT:
				case GL_INT:
				case GL_UNSIGNED_INT:
					glVertexAttribIPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset*sizeof(T)));
					break;
				case GL_HALF_FLOAT:
				case GL_FLOAT:
				case GL_FIXED:
				case GL_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_10F_11F_11F_REV:
					glVertexAttribPointer(loc, size, type, normalized, stride * sizeof(T), reinterpret_cast<T*>(offset*sizeof(T)));
					break;
				case GL_DOUBLE:
					glVertexAttribLPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset*sizeof(T)));
					break;
			}
		}
		GLint getUniformLocation(const std::string &name)
		{
			return glGetUniformLocation(program, name.c_str());
		}
	private:
		GLuint program = 0;
	};
}

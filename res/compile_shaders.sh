#!/bin/sh

glslangValidator -G460 shader.vert shader.frag
spirv-link vert.spv frag.spv -o shader.spv
spirv-opt -O shader.spv -o shader_opt.spv

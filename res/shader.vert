#version 460
layout(location = 0) in vec3 vCol;
layout(location = 1) in vec3 vPos;
layout(location = 2) in vec2 texPos;
layout(location = 3) out vec3 color;
layout(location = 4) out vec2 texPosOut;

layout(location = 5) uniform mat4 Projection;
layout(location = 6) uniform mat4 View;
layout(location = 7) uniform mat4 Model;

layout(binding = 0, std140) buffer mainBuffer
{
	vec4 positions[];
};

void main()
{
	gl_Position = Projection *
	View * mat4(vec4(1, 0, 0, 0),
			vec4(0, 1, 0, 0),
			vec4(0, 0, 1, 0),
			positions[gl_InstanceID]) * 
	Model * vec4(vPos, 1.0);
	color = vCol;
	texPosOut = texPos;
}

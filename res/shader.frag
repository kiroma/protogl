#version 460
layout(location = 3) in vec3 color;
layout(location = 4) in vec2 texPos;
layout(location = 0) out vec4 FragColor; //Note: Has to be in location 0

uniform sampler2D myTexture;

void main()
{
	FragColor = vec4(color, 1.0f) * texture(myTexture, texPos);
}
